package com.example.workmanagertest

import android.app.Application
import androidx.room.Room
import androidx.work.WorkManager
import com.example.workmanagertest.data.repository.DataRepository
import com.example.workmanagertest.data.repository.SettingsRepository
import com.example.workmanagertest.data.datasource.local.CommonStoreImpl
import com.example.workmanagertest.data.datasource.local.GitHubDatabase
import com.example.workmanagertest.data.datasource.local.LocalGitHubDataSource
import com.example.workmanagertest.data.datasource.network.GitHubService
import com.example.workmanagertest.data.repository.WorkerDataRepository
import com.example.workmanagertest.data.worker.UpdateEnqueueHelper

class WorkManagerApplication : Application() {

    private val commonStore by lazy { CommonStoreImpl(this) }

    private val database by lazy {
        Room.databaseBuilder(
            applicationContext,
            GitHubDatabase::class.java, "database-name"
        ).build()
    }

    val dataRepository: DataRepository by lazy {
        DataRepository(
            GitHubService.create(),
            LocalGitHubDataSource(database.getDao())
        )
    }

    val settingsRepository: SettingsRepository by lazy {
        SettingsRepository(commonStore)
    }

    val workerDataRepository: WorkerDataRepository by lazy {
        WorkerDataRepository(commonStore)
    }

    val workManagerHelper: UpdateEnqueueHelper by lazy {
        UpdateEnqueueHelper(settingsRepository, WorkManager.getInstance(this), workerDataRepository)
    }
}