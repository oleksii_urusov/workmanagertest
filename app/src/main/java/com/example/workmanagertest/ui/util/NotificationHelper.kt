package com.example.workmanagertest.ui.util

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.workmanagertest.R
import com.example.workmanagertest.WorkManagerApplication
import com.example.workmanagertest.data.repository.SettingsRepository

const val VERBOSE_NOTIFICATION_CHANNEL_NAME = "Verbose WorkManager Notifications"
const val VERBOSE_NOTIFICATION_CHANNEL_DESCRIPTION = "Shows notifications whenever work starts"
const val CHANNEL_ID = "VERBOSE_NOTIFICATION"
const val DEFAULT_NOTIFICATION_ID = 1

class NotificationHelper(
    private val applicationContext: Context,
    private val title: String,
    private val notificationId: Int = DEFAULT_NOTIFICATION_ID
) {
    private val Context.notificationManager
        get() = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

    private val settingsRepository: SettingsRepository =
        (applicationContext as WorkManagerApplication).settingsRepository

    fun showStartedNotification() {
        showNotification("Started...")
    }

    fun showSuccessNotification() {
        showNotification("Success!")
    }

    fun showFailureNotification() {
        showNotification("Failed!")
    }

    fun showNotification(message: String) {
        if (!settingsRepository.showNotifications) return

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = createNotificationChannel()
            applicationContext.notificationManager.createNotificationChannel(channel)
        }

        val notification =
            createNotification(applicationContext, message)
        NotificationManagerCompat.from(applicationContext).notify(notificationId, notification)
    }

    private fun createNotification(context: Context, message: String) =
        NotificationCompat.Builder(context, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentTitle(title)
            .setContentText(message)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setVibrate(LongArray(0))
            .build()

    fun hideStatusNotification(context: Context) = context.notificationManager.cancel(notificationId)

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(): NotificationChannel {
        val name = VERBOSE_NOTIFICATION_CHANNEL_NAME
        val importance = NotificationManager.IMPORTANCE_HIGH
        return NotificationChannel(CHANNEL_ID, name, importance).apply {
            description = VERBOSE_NOTIFICATION_CHANNEL_DESCRIPTION
        }
    }
}
