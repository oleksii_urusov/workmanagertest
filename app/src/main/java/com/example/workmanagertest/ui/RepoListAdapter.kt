package com.example.workmanagertest.ui

import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.example.workmanagertest.data.model.GitHubRepoData

private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<GitHubRepoData>() {
    override fun areItemsTheSame(oldItem: GitHubRepoData, newItem: GitHubRepoData) = oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: GitHubRepoData, newItem: GitHubRepoData) = oldItem == newItem
}

class RepoListAdapter : ListAdapter<GitHubRepoData, RepoViewHolder>(DIFF_CALLBACK), Filterable {
    private var originalItems: List<GitHubRepoData>? = null

    private val filterObject: Filter by lazy {
        object : Filter() {
            override fun performFiltering(constraint: CharSequence?) =
                FilterResults().apply { values = filterItems(constraint) }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                results?.let {
                    submitInternally(it.values as List<GitHubRepoData>)
                }
            }
        }
    }

    private fun submitInternally(list: List<GitHubRepoData>) = super.submitList(list)

    fun filter(filterString: String?) {
        filterObject.filter(filterString)
    }

    override fun submitList(list: List<GitHubRepoData>?) {
        super.submitList(list)
        originalItems = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepoViewHolder = RepoViewHolder.create(parent)

    override fun onBindViewHolder(holder: RepoViewHolder, position: Int) = holder.bind(getItem(position))

    override fun getFilter(): Filter = filterObject

    private fun filterItems(constraint: CharSequence?): List<GitHubRepoData> = if (constraint?.length == 0) {
        originalItems ?: emptyList()
    } else {
        originalItems?.filter {
            val lowerCaseConstraint = constraint.toString().toLowerCase()
            it.language
                ?.toLowerCase()
                ?.contains(lowerCaseConstraint)
                ?: false
        } ?: emptyList()
    }
}