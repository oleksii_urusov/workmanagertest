package com.example.workmanagertest.data.datasource.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.workmanagertest.data.model.GitHubRepoData
import com.example.workmanagertest.data.model.ProgrammingLanguage
import com.example.workmanagertest.data.model.Page

@Dao
interface GitHubDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun putRepos(posts: List<GitHubRepoData>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun setLasLoadedPage(page: Page)

    @Query("SELECT * FROM lastLoadedPage WHERE id LIKE 0")
    fun getLastLoadedPage(): Page?

    @Query("SELECT * FROM repos")
    fun getRepos(): LiveData<List<GitHubRepoData>>

    @Query("SELECT * FROM repos")
    fun getReposSync(): List<GitHubRepoData>

    @Query("SELECT * FROM repos WHERE id IN (:ids)")
    fun getReposByIdSync(ids: List<Long>): List<GitHubRepoData>

    @Query("SELECT * FROM language")
    fun getLanguages(): LiveData<List<ProgrammingLanguage>>

    @Query("SELECT * FROM language")
    fun getLanguagesSync(): List<ProgrammingLanguage>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun putLanguages(languages: List<ProgrammingLanguage>)
}