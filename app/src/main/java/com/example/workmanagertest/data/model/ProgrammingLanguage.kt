package com.example.workmanagertest.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "language")
data class ProgrammingLanguage(
    @PrimaryKey val name: String
)