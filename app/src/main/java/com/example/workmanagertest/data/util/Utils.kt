package com.example.workmanagertest.data.util

import androidx.work.BackoffPolicy
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.ExistingWorkPolicy
import androidx.work.OneTimeWorkRequest
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkManager
import androidx.work.WorkRequest
import java.util.concurrent.TimeUnit

fun simulateLongRunningWork() = Thread.sleep(3000L)

fun WorkManager.enqueueUnique(workRequest: WorkRequest, uniqueWorkName: String) {
    when (workRequest) {
        is OneTimeWorkRequest -> enqueueUniqueWork(uniqueWorkName, ExistingWorkPolicy.REPLACE, workRequest)
        is PeriodicWorkRequest -> enqueueUniquePeriodicWork(
            uniqueWorkName,
            ExistingPeriodicWorkPolicy.REPLACE,
            workRequest
        )
    }
}

fun <B : WorkRequest.Builder<*, *>, K : WorkRequest> WorkRequest.Builder<B, K>.setDefaultBackoffCriteria(): B =
    setBackoffCriteria(
        BackoffPolicy.EXPONENTIAL,
        OneTimeWorkRequest.DEFAULT_BACKOFF_DELAY_MILLIS,
        TimeUnit.MILLISECONDS
    )