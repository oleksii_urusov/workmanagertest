package com.example.workmanagertest.data.repository

import android.util.Log
import androidx.lifecycle.LiveData
import com.example.workmanagertest.data.datasource.local.LocalGitHubDataSource
import com.example.workmanagertest.data.model.GitHubRepoData
import com.example.workmanagertest.data.datasource.network.GitHubService
import com.example.workmanagertest.data.model.ProgrammingLanguage

private const val PAGE_SIZE = 2
private const val REPO_QUERY = "Android"

private val TAG = DataRepository::class.java.simpleName

class DataRepository(private val gitHubService: GitHubService, private val localDb: LocalGitHubDataSource) {

    // Just a silly method that simulates a "sync" of latest data. In fact we're just getting repos page by page
    // as if new ones appeared after we last "synced"
    fun updateAndGetNewRepos(): List<GitHubRepoData>? = try {
        val pageNumber = localDb.getLastLoadedPageNumber() + 1
        val response = gitHubService.searchRepos(
            REPO_QUERY, pageNumber,
            PAGE_SIZE
        ).execute()

        response.body()?.items?.also {
            localDb.putRepos(it)
            localDb.setLastLoadedPageNumber(pageNumber)
        }
    } catch (t: Throwable) {
        Log.d(TAG, "updateAndGetNewRepos: $t")
        null
    }

    fun getRepos(): LiveData<List<GitHubRepoData>> = localDb.getRepos()

    fun getReposSync(): List<GitHubRepoData> = localDb.getReposSync()

    fun getReposByIdSync(ids: List<Long>) = localDb.getReposByIdSync(ids)

    fun addLanguages(languages: List<ProgrammingLanguage>) = localDb.addLanguages(languages)

    fun getLanguages() = localDb.getLanguages()
    fun getLanguagesSync() = localDb.getLanguagesSync()
}