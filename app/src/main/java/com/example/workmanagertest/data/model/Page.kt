package com.example.workmanagertest.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "lastLoadedPage")
data class Page(
    @PrimaryKey @field:SerializedName("id") val id: Long,
    val pageNumber: Int
) {
    companion object {
        fun create(pageNumber: Int) = Page(0, pageNumber)
    }
}