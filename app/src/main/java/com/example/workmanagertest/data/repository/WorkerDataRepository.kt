package com.example.workmanagertest.data.repository

import com.example.workmanagertest.data.datasource.local.CommonStore

class WorkerDataRepository(private val commonStore: CommonStore) {

    var reposUpdateWorkId: String?
        get() = commonStore.reposUpdateWorkId
        set(value) {
            commonStore.reposUpdateWorkId = value
        }

    var languagesUpdateWorkId: String?
        get() = commonStore.languagesUpdateWorkId
        set(value) {
            commonStore.languagesUpdateWorkId = value
        }
}