package com.example.workmanagertest.data.worker

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import com.example.workmanagertest.WorkManagerApplication
import com.example.workmanagertest.data.repository.DataRepository
import com.example.workmanagertest.data.repository.SettingsRepository
import com.example.workmanagertest.data.util.simulateLongRunningWork
import com.example.workmanagertest.ui.util.NotificationHelper

private val TAG = UpdateGithubReposWorker::class.java.simpleName

private const val NOTIFICATION_TITLE = "Repo update"

/**
 * Updates GitHub repos list
 */
class UpdateGithubReposWorker(applicationContext: Context, parameters: WorkerParameters) :
    Worker(applicationContext, parameters) {
    private val notificationHelper = NotificationHelper(applicationContext, NOTIFICATION_TITLE)

    private val dataRepository: DataRepository = (applicationContext as WorkManagerApplication).dataRepository
    private val settingsRepository: SettingsRepository =
        (applicationContext as WorkManagerApplication).settingsRepository

    override fun doWork(): Result {
        notificationHelper.showStartedNotification()
        Log.d(TAG, Thread.currentThread().name)

        simulateLongRunningWork()

        val newRepos = dataRepository
            .updateAndGetNewRepos()
            ?.map { it.id }
            ?.toLongArray()
        val success = newRepos != null
        if (success) notificationHelper.showSuccessNotification() else notificationHelper.showFailureNotification()
        return when {
            success -> Result.success(
                workDataOf(KEY_NEW_REPOS_IDS to newRepos)
            )
            settingsRepository.doRetryUpdate -> Result.retry()
            else -> Result.failure()
        }
    }
}