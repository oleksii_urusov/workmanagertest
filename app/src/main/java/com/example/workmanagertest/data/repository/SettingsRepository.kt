package com.example.workmanagertest.data.repository

import com.example.workmanagertest.data.datasource.local.CommonStore

class SettingsRepository(private val commonStore: CommonStore) : CommonStore by commonStore {
    fun makeWorkRecurring(recurringIntervalSeconds: Int) {
        commonStore.isRecurring = true
        commonStore.repeatIntervalMinutes = recurringIntervalSeconds
    }

    fun makeWorkAOneTime() {
        commonStore.isRecurring = false
        commonStore.repeatIntervalMinutes = 0
    }
}