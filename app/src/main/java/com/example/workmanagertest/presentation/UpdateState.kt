package com.example.workmanagertest.presentation

sealed class UpdateState(val message: String, val isFinished: Boolean) {
    object Waiting : UpdateState("waiting", false)
    object Updating : UpdateState("updating...", false)
    object Updated : UpdateState("updated!", true)
    object Failed : UpdateState("failed :(", true)
    object Cancelled : UpdateState("cancelled >_>", true)
    object Idle : UpdateState("idle -_-", true)
}